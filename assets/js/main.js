function buyTicket() {
  document.querySelector(".modal-wrapper").classList.add("open");
}

function closeModal() {
  document.querySelector(".modal-wrapper").classList.remove("open");
}

const elClose = document.querySelectorAll(".closeModal");
for (const btnClose of elClose) {
  btnClose.addEventListener("click", closeModal);
}

const elOpen = document.querySelectorAll(".btn-ticket");
for (const btnBuyTicket of elOpen) {
  btnBuyTicket.addEventListener("click", buyTicket);
}
